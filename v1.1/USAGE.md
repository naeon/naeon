Usage: naeon [options [parameters]]

Options:

Backup of file or directory
---------------------------
naeon -b|--backup [file(s)] [-k|--keep] [-u|--unattended]

To backup a file: naeon -b /home/user/documents/document.doc
To backup a group of files using a wildcard: naeon -b /home/user/documents/john*.doc

To backup a directory: naeon -b /home/user/documents/
To backup a directory, unattended: naeon -b /home/user/documents/ -u


Backup from a plaintext file containing a list of files, one file per line.
---------------------------------------------------------------------------
naeon -l|--list [file list(s)] [-k|--keep] [-u|--unattended]

To backup from a file list: naeon -l /home/user/documents/list_of_files.txt


To restore:
-----------
naeon -r [ one or more project IDs: yyyymmddhhmmss ] [ project IDs using wildcard: naeon-yyyymm* ]
Note: place the project directories ('naeon-yyyymmddhhmmss') you wish to restore, in your working directory (see naeon.conf).


To keep your compressed archive after backing up or restoring:
--------------------------------------------------------------
Add the option '-k' or '--keep' to your backup or restore command.


To keep your compressed archive compressed after restoring:
--------------------------------------------------------------
Add the option '-n' or '--nodecompress' to your restore command.


To run naeon in unattended mode:
--------------------------------
Add the option '-u' or '--unattended'. Works in backup (file, directory), restore, and list mode.


Help
----
naeon -h|--help

