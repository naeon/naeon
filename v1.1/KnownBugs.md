# Known bugs for naeon

This file is a listing of all known bugs in this package.

+   Get rid of the minimum filesize of 100,000 bytes
+	While creating a backup, _temp* and backup.tar.gz files should not be stored in current directory but in the working directory instead
+	

