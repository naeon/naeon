<< ////

   naeon is an unhackable, uncrackable, quantum-proof data encryption tool.

   Copyright (C) 2020-2021 Rene Smaal
   Copyright (C) 2021- naeon contributors

   This file is part of naeon.

   Naeon is free software. You can redistribute it and/or modify it under the
   terms of the GNU Affero General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option)
   any later version.

   Naeon is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
   more details.

   The latest version of this license is in https://www.gnu.org/licenses/
   and version 3 or later is part of all distributions of naeon version
   0.3.0 or later.

////

________________________________________________________________________________

	N A E O N    C O N F I G U R A T I O N
________________________________________________________________________________

> Note that all timestamps are in UTC (Universal Coordinated Time) and not in the local time zone.
> Only alphanumeric characters will pass.

> Your name - or name of the organization - issuing the backup:
ISSUER=

> Any issuer-related notes you want to attach to the backup:
ISSUER_NOTES=

> Working directory (full path - leave empty for current directory):
WORKING_DIR=

> If the data storage provider supports it, Public Chunks will be deleted on successful retrieval (no/yes):
DELETE_ON_RETRIEVAL=no

> If the data storage provider supports it, Public Chunks will be blocked for retrieval up until this time (yyyymmddhhmmss):
EMBARGO_TIMESTAMP=20200101000000

> If the data storage provider supports it, Public Chunks will self-destruct at (yyyymmddhhmmss):
SELFDESTRUCT_TIMESTAMP=99991231235959

> Description – whether cryptic or not so that only you can make sense of it – of where the Public Chunks are stored:
LOCATION_PUBLIC_CHUNKS=


