## March 2021

### v1.0.1 - 2021-03-06

### Added
+ In backup mode, after adding append block, check if the size of prepend, data, and append block add up to the reported total size.
+ Check if all public chunks are similar in size. If the unlikely case this happens (usually with very big archives), exit the program.
+ Notification of successful decryption.

### Changed
+ Default timestamp (when not set in naeon.conf) from 99990101010101 to 99991231235959.

## February 2021

### v1.0 - 2021-02-20

### Added
+ Check on the version of tar: this should be 'GNU tar' as bsdtar (standard on macOS nowadays) displays unwanted behaviour when used icw relative pathnames.
+ GNU tar to the list of prerequisites.
+ Extra information in the explanatory text describing the proceedings after a backup has been made.
+ Better reporting in restore mode - now mentioning removing prepend and append block.
+ Function check_datestring checks validity of Embargo Timestamp and Selfdestruct Timestamp in naeon.conf.
+ If no Embargo Timestamp and Selfdestruct Timestamp are set in naeon.conf, they will be set to their defaults of 20200101000000 and 99990101000000 respectively.
+ Check if Embargo Timestamp precedes Selfdestruct Timestamp and if not, exit.

### Changed
+ Text "Found:" into "Selected:" since it concerns submitted project IDs, which does not necessarily mean valid (read: found) ones.
+ Tidied up code, changelog.
+ Moved the tag <Naeon_version> to the top of the Master Key XML.

### v0.9 - 2021-02-09

### Added
+ Restore mode: use of wildcards (f.e.: naeon --restore naeon-2021* or naeon --restore 20210202153948 20210202180839 20210202191220 -u -n).
+ Backup mode: now reporting "Creating compressed archive" when the tarball is being built. Since this can take some time, user might otherwise wonder what's happening.
+ Restore mode: clean exit with error message in case an invalid key was used for decryption (should normally never happen with the right, non-corrupted Master Key XML).
+ Restore mode: now reporting "Processing naeon project .. of .." for each project (to be) restored.
+ Clean exit with error message in case encryption of the input file failed.
+ Option '--batch' added to the encryption command since gpg requires it when used in combination with the '--passphrase' option.
+ All fatal errors now marked in red for better visual distinction between fatal errors and standard output.
+ At the start of the program in restore mode, naeon now displays "Found: " followed by the selected project ID(s).

### Changed
+ Restore mode: better, more informative error message in case the hash of private and/or public chunks do not match those in the XML.
+ Passphrase 128 characters alphameric.
+ Renamed the tag <Key> in the Master Key XML to <Passphrase> which is what it in fact is: a passphrase used by the gpg command to generate the 256bit key.
+ Due to the renaming of the <Key> tag in the Master Key XML, the restore mode in this version is not backwards compatible with prior versions.
+ Moved major part of initializing block to before the preliminary checks.
+ Moved the passphrase tag in the Master Key XML to just after the <Encryption_command> tag.
+ The encryption key (passphrase) is no longer (temporarily) saved to disk, but (temporarily) kept in memory instead, and immediately cleared after use.

### Fixed
+ Restore mode: now skipping submitted non-existing project IDs, instead of exiting the program.
+ Restore mode: when restoring more than one project ID, the naenon header is now shown only once.
+ Restore mode: instead of crashing, naeon now skips to the next project ID in case a submitted project ID is missing directories and/or required files.
+ Backup mode: "Creating compressed archive" was displayed one time too many.
+ Created more consistency in the (position of) displayed messages in batch restore mode.
+ Restore mode: improved error handling in cases where private and/or public chunks are missing, checksums do not match, or decryption was unsuccessful.
+ Program exited unintentionally in restore mode when using the --unattended option.

### Removed
+ Command 'file' from prerequisites. On systems where it is not installed, the validity check on plaintext file lists is skipped when backing up with parameter '--list'.

### v0.8 - 2021-02-02

### Added
+ Error log in backup mode, saved in the private directory under the name 'naeon-yyyymmddhhmmss-ErrorLog.txt'.
+ Encryption program version to the Master Key XML.
+ Restore mode now accepts input parameter, being the naeon project ID (yyyymmddhhmmss) with or without prefix 'neon-'. No parameter needed in case one project exists.
+ libseccomp to requirements. In restore mode, the command 'file' (part of libseccomp) is needed in order to determine the file type of input file lists.
+ Version numbers of required software packages.

### Changed
+ Renamed directories for compressed and decompressed (restored) data to their more logical names 'naeon-<yyyymmddhhmmss>-(de)compressed'.
+ Variable name "NO_DECOMPRESS" into "DECOMPRESS".
+ Options '--file' and '--dir' merged into '--backup'.
+ Renamed folders to: naeon-<yyyymmddhhmmss>-decompressed, -compressed, -private, -public.
+ In Error Log, ignore tar messages 'tar: Removing leading `/' from member names'.
+ In restore mode, forward slashes removed from input parameter, so file completion will also work (when input parameter ends with a forward slash).
+ In restore mode, the integrity check now displays a file counter instead of the hashes. Only in case of errors, the hash of the file in question will be displayed.
+ 'tail -c' and 'head -c' replaced by 'tail --bytes' and 'head --bytes' for reasons of portability.
+ In backup mode '--list', error message adjusted when input file doesn't pass the plaintext check.
+ Moved the declaration of required software packages from backup and restore blocks to init block.

### Fixed
+ Removed rendundant message "Restored decompressed data are stored in" as this was already mentioned 3 lines before.
+ Wildcards can now be used in all modes (Tim vd A).
+ Some sort of non posix compliancy caused 'echo' icw the '-ne' parameter not to be working on macOS systems. Switched to printf in these instances.

## January 2021

### v0.7 - 2021-01-25

### Added
+ Support for wildcards on command line (options --file, --dir).
+ Relative pathnames for files, directories, and file list now supported.
+ Clean exit with status 1 when no input parameters are supplied.
+ After backup, a list of files is now kept in the private directory, named 'naeon_filelist_<yyyymmddhhmmss>.txt'.

### Changed
+ In backup mode, creation of temporary directory moved from 'Generating encrytpion key' to 'Files & directories'.
+ Files, directories and file lists should now be entered between double quotes.
+ Moved private, public, and restored directory into a new 'project' directory, named 'naeon-<yyyymmddhhmmss>'.
+ Naeon file list in public directory now a detailed listing of all files in archive, with timestamp of files and their sizes.

### Fixed
+ Failed to backup a file which contains spaces in the name in 'backup from file' mode.
+ Sending tar message 'Removing leading `/' from member names' to /dev/null since restores are done in a separate directory anyway.
+ Temporary tar files were not stored in the temporary directory.
+ Help texts updated.
+ On macOS systems, the tar command doesn't allow to create a new tarball with the -uvf option.
+ In backup mode, when minimum file size was reached, program exited without removing empty private and public directories.
+ Now performing a check on empty private / public directories in restore mode.
+ When option '--no_decompress' was chosen and therefore no restored directory was created, "Restored data are stored in ..." was displayed.

### v0.6 - 2021-01-21

### Added
+ Check on unsupported input fags / arguments in parsing routine.
+ All output goes to the working directory (by default this is the current directory).
+ Working directory can be set in the configuration file.
+ Error handling routine for non-successful creation of directories.
+ New: '--list' for backup from plaintext file containing a file list: one file per line, with full pathname, no wildcards.
+ Check on use of relative pathnames (tar doesn't like them when restoring, for security reasons).

### Changed
+ In restore mode, no creation of directory 'restored-##########' if it already exists.
+ In restore mode, when option '--keep' is chosen, compressed archive is stored in the 'restored-##########' instead of current directory.
+ Program directory and working directory can now be two different directories.
+ Updated help text both in program and in README.md.
+ Renamed some variables to more logical names.
+ Creation of restore directory moved to preliminary checks.
+ In attended restore mode, create temporary & restore directory only after user confirms the start of the process.
+ Compacted output to the screen as much as possible, to improve readability and reduce clutter.

### Fixed
+ Wrong use of boolean in cleaning up compressed archive routine.
+ Now checking on conflicting input parameters.
+ Crashed when run from a pathname containing one or more spaces.

### Removed
+ Redundant check on zero input arguments, as the new parsing routine takes care of that as well.

### v0.5 - 2021-01-18

### Added
+ Removal of compressed archive after successful backup, except when option -k is chosen.
+ Removal of compressed archive after successful restore, except when option -k is chosen AND/OR DECOMPRESS_ON_RESTORE is set to no.
+ Input argument '-k' or '--keep' to keep the compressed archive after backup or restore.
+ Function 'show_usage' in init mode, called when no valid arguments were specified.

### Changed
+ Improved, less cryptic, error message on unsuccessful attempt to create compressed archive.
+ Moved startup message 'Starting naeon in .. mode' to the end of intro text.
+ After restore, setting the (UTC) timestamp of the backup instead of that of input file.
+ Moved from configuration file to input argument: unattended mode, no decompress on restore.
+ Argument parsing routine simplified.

### Fixed
+ Backup using wildcards.
+ Suppress tar " Removing leading `/' " warnings in backup mode.
+ Error message 'gpg: invalid option' when generated key started with '-'.
+ On several occasions where the program exited with exit code 1, the compressed archive wasn't cleaned up.
+ User input from configuration file sanitized in order to avoid non-alphanumeric characters pass through to the Master_Key.xml file.

### Removed
+ Inputfile timestamp variable, as the inputfile is no longer necessarily a single file but can also be a directory or group of files.
+ Tag <Inputfile_timestamp> from Master_Key.XML.
+ Input parameter check at the start of the backup mode, as it is done at a later moment.
+ Redundant / commented out code blocks.

### v0.4 - 2021-01-16

### Added
+ Explanatory texts in configuration file to clarify environment variables.
+ MANIFEST.md.
+ Argument parsing routine rewritten in order to support long options or mixing options and positional arguments together.
+ Argument to display help text (-h, --help).
+ Backup mode no more limited to one file. Instead, directories are now also valid as inputs. No wildcards yet.
+ In backup mode, the input file(s) will be tarballed-gzipped before being encrypted.
+ Option in configuration file to decompress the compressed archive after restore (no/yes, default no).

### Changed
+ Converted plaintext files to Markdown format.
+ Backup mode is now invoked by using an input argument (-b or --backup) before the filename(s).

### Removed
+ Check on empty private & public directories, as they will be cleanly created with timestamp anyway.
+ The obligation to have your inputfile in the current directory.

### v0.3 - 2021-01-14

### Added
+ readme.txt.
+ Warranty text in backup as well as restore mode.
+ 'Haiku' (a BeOS clone) added to the list of supported platforms (linux, macOS, BSD).
+ GNU Affero General Public License.
+ This CHANGELOG file to hopefully serve as an evolving example of a standardized open source project NAEON.

### Changed
+ Text color yellow (tput setaf 226) changed into green (tput setaf 10) for better readability on white backgrounds.

### Fixed
+ Misspelling of 'macOS'.
+ Error 'Argument list too long' when moving large numbers of Public Chunks to public directory on low memory systems.
+ Missing requirements in backup mode.
+ Variable 'unattended mode' was not set in restore mode.

### v0.2 - 2021-01-10

### Added
+ Minimum size for inputfile (100,000 bytes) in order to prevent encryption key ending up in public chunk.
+ Reporting on updates in size of encrypted file after: creation, insertion of prepend block, and adding append block.
+ Handling the unlikely occurrence of a zero-size append block.
+ Better, more structured reporting on the sizes of the chunks and their distribution.
+ Tag in XML: 'Naeon_version' containing the current naeon version number.

### Changed
+ Replaced 'exit 0' by 'exit 1' when program is terminated unsuccessfully.
+ Size of prepend block: from a random value between 1,000-500,000 bytes, to a random value between 1-10,000 bytes.

### Fixed
+ On BSD and macOS: when splitting the encrypted file into private and public chunks, 'bc' was not set to arithmetic mode.
+ On BSD and macOS: splitting encrypted file into > 676 (26^2) parts ( x[a-z][a-z] ) resulted into error "split: too many files". Fixed: x[a-z][a-z][a-z] now maxes out on 17,576 (26^3) parts.
+ Find command adjusted - now looking only for split-files x??? and no more for x?? and x?????.

### v0.1 - 2021-01-08

### Added
+ Merged ecbu (backup program) and ecre (restore program) into 'naeon'.
+ First integrated, fully functioning, encrypted backup & restore version of this program. Initial public beta version.

### [PRE VERSION 0.1]

+ ecre v0.8 - 2021-01-08
+ ecre v0.7 - 2021-01-08
+ ecbu v0.1h - 2021-01-08
+ ecre v0.6 - 2021-01-07
+ ecre v0.5 - 2021-01-07
+ ecre v0.4 - 2021-01-07
+ ecbu v0.1g - 2021-01-06
+ ecre v0.3 - 2021-01-05
+ ecre v0.2 - 2021-01-05
+ ecre v0.1 - 2021-01-05
+ ecre v0.0 - 2021-01-05

## December 2020

+ ecbu v0.1f - 2020-12-31
+ ecbu v0.1e - 2020-12-30
+ ecbu v0.1d - 2020-12-30
+ ecbu v0.1c - 2020-12-29
+ ecbu v0.1b - 2020-12-29
+ ecbu v0.1a - 2020-12-29
+ ecbu v0.0 - 2020-12-27 / 2020-12-29
+ conceptualization - 2020-12-14 / 2020-12-26



