# Manifest for naeon

This file is a listing of all files considered to be part of this package.

* naeon
* naeon.conf
* KnownBugs.md
* LICENSE.txt
* MANIFEST.md
* README.md
* USAGE.md
