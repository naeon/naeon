<< ////

   naeon is an unhackable, uncrackable, quantum-proof data encryption tool.

   Copyright (C) 2020-2021 Rene Smaal
   Copyright (C) 2021- naeon contributors

   This file is part of naeon.

   Naeon is free software. You can redistribute it and/or modify it under the
   terms of the GNU Affero General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option)
   any later version.

   Naeon is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
   more details.

   The latest version of this license is in https://www.gnu.org/licenses/
   and version 3 or later is part of all distributions of naeon version
   0.3.0 or later.

////

Usage: naeon [options [parameters]]

Options:

Backup of file or directory
---------------------------
naeon -b|--backup [file(s)] [-k|--keep] [-u|--unattended]

To backup a file: naeon -f /home/user/documents/document.doc
To backup a group of files using a wildcard: naeon -f /home/user/documents/john*.doc

To backup a directory: naeon -d /home/user/documents/
To backup a directory, unattended: naeon -d /home/user/documents/ -u


Backup from a plaintext file containing a list of files, one file per line.
---------------------------------------------------------------------------
naeon -l|--list [file list(s)] [-k|--keep] [-u|--unattended]

To backup from a file list: naeon -l /home/user/documents/list_of_files.txt


To restore:
-----------
naeon -r [ one or more project IDs: yyyymmddhhmmss ] [ project IDs using wildcard: naeon-yyyymm* ]
Note: place the project directories ('naeon-yyyymmddhhmmss') you wish to restore, in your working directory (see naeon.conf).


To keep your compressed archive after backing up or restoring:
--------------------------------------------------------------
Add the option '-k' or '--keep' to your backup or restore command.


To keep your compressed archive compressed after restoring:
--------------------------------------------------------------
Add the option '-n' or '--nodecompress' to your restore command.


To run naeon in unattended mode:
--------------------------------
Add the option '-u' or '--unattended'. Works in backup (file, directory), restore, and list mode.


Help
----
naeon -h|--help


