# Manifest for naeon

This file is a listing of all files considered to be part of this package.

* README.md
* LICENSE.txt
* MANIFEST.md
* CHANGELOG.md
* KnownBugs.md
* naeon
* naeon.conf


