# Known bugs for Naeon

This file is a listing of all known bugs in this package.

+ Does not allow directory names set in naeon.conf to contain spaces.
+ Program exits (gracefully) in case the filesize of the compressed archive is below 100,000 bytes, ruling out processing real small amounts of data.
