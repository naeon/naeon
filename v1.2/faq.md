# Frequently Asked Questions for naeon

## How do I install Naeon?

* You can unpack the .tar.gz file in any directory you like.
* Make sure that this directory is added to your PATH environment variable ($PATH).

## Which Naeon files do I need?

* You only need the following two files: naeon and naeon.conf.

## On MacOS, I get error messages referring to missing commands. How do I fix this?

* Install homebrew. Use the [following link](https://brew.sh/).
* Then open a terminal and execute the following command: brew install coreutils gawk gnu-tar gnutls gpg

## I have an issue that isn't answered above. Where can I find help?

* For information on the use of Naeon, simply start Naeon with the option -h (or --help)
* Visit the Naeon Discussion Forum on our [SourceForge project page](https://sourceforge.net/p/naeon/discussion/)
