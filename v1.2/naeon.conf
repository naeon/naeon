<< ////

   Naeon is an unhackable, uncrackable, quantum-proof data encryption tool.

   Copyright (C) 2020-2022 Rene Smaal
   Copyright (C) 2021- Naeon contributors

   This file is part of Naeon.

   Naeon is free software. You can redistribute it and/or modify it under the
   terms of the GNU Affero General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option)
   any later version.

   Naeon is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
   more details.

   The latest version of this license is in https://www.gnu.org/licenses/
   and version 3 or later is part of all distributions of Naeon version
   0.3.0 or later.

////

________________________________________________________________________________

	N A E O N    C O N F I G U R A T I O N
________________________________________________________________________________

# Note that ALL TIMESTAMPS ARE IN UTC (Universal Time Coordinated) and not in the local time zone.
# Only alphanumeric characters will pass.

# Naeon project directory (FULL PATH WITHOUT SPACES OR TRAILING SLASH, leave empty for current directory):
NAEON_DIR=

# Naeon temp directory (FULL PATH WITHOUT SPACES OR TRAILING SLASH, leave empty for current directory):
NAEON_TEMP_DIR=

# Your name — or name of the organization — issuing the backup:
ISSUER=NAEON

# Any issuer-related notes you want to attach to the backup:
ISSUER_NOTES=

# If the data storage provider supports it, Public Chunks will be deleted on successful retrieval (no/yes):
DELETE_ON_RETRIEVAL=no

# If the data storage provider supports it, Public Chunks will be blocked for retrieval up until this time (yyyymmddhhmmss):
EMBARGO_TIMESTAMP=20200101000000

# If the data storage provider supports it, Public Chunks will self-destruct at (yyyymmddhhmmss):
SELFDESTRUCT_TIMESTAMP=29991231235959

# Number of parts the Private Chunk should be split up into (any integer from 1 to 999, default is 1):
SPLIT_PRIVATE_FOLDER_INTO_PARTS=

# Description – whether cryptic or not so that only you can make sense of it – of where the Public Chunks are stored:
LOCATION_PUBLIC_CHUNKS=

# If set to "yes", a uuencoded version of both the private chunk and the Master Key XML, will be saved in the private dir (no/yes, default = no):
PAPER_WALLET=

# If set to "yes", the project description will not only be stored in the XML, but in a text file in the private dir as well (no/yes, default=no):
STORE_PROJECTDESC_SEPARATELY=

