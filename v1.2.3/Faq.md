# Frequently Asked Questions for naeon

## How do I install Naeon on Unix-like systems (Linux, macOS, BSD, Haiku)?

* You can unpack the .tar.gz file in any directory you like.
* Make sure that this directory is added to your PATH environment variable ($PATH).
* On macOS, there are two more steps:
* 1. Install homebrew. Use the [following link](https://brew.sh/).
* 2. Open a terminal and execute the following command: brew install coreutils gawk gnu-tar gnutls gpg

## How do I install Naeon on Windows (Cygwin)?

* Download Cygwin from www.cygwin.com and run setup.exe.
* Click Next through the defaults and select mirror for downloading packages.
* Select latest versions of the following packages: bc wget2 gnupg2 sharutils coreutils
* Open Cygwin terminal.
* Install apt-cyg:
* wget2 https://raw.githubusercontent.com/transcode-open/apt-cyg/master/apt-cyg
* chmod +x apt-cyg
* mv apt-cyg /usr/local/bin

* Install naeon:

* cd /usr/local/bin
* wget2 https://sourceforge.net/projects/naeon/files/latest/download
* mv download naeon.tar.gz
* tar xvfz naeon.tar.gz
* cd

## How to start Naeon?
* To start naeon in help mode:
* naeon -h

## Which Naeon files do I need?

* You only need the following two files: naeon and naeon.conf.

## On MacOS, I get error messages referring to missing commands. How do I fix this?

* Install homebrew. Use the [following link](https://brew.sh/).
* Then open a terminal and execute the following command: brew install coreutils gawk gnu-tar gnutls gpg

## I have an issue that isn't answered above. Where can I find help?

* For information on the use of Naeon, simply start Naeon with the option -h (or --help)
* Visit the Naeon Discussion Forum on our [SourceForge project page](https://sourceforge.net/p/naeon/discussion/)
