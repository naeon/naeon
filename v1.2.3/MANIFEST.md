# Manifest for Naeon

This file is a listing of all files considered to be part of this package.

* naeon
* naeon.conf
* Faq.md
* KnownBugs.md
* MANIFEST.md
* README.md
* LICENSE.txt
* SHA512SUMS
